package com.teknei.bid.service.remote;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(url = "http://201.163.237.4:28080/vision-proxy/", name = "visionClient")
public interface ByPassCaller {

    @RequestMapping(method = RequestMethod.POST, value = "images:annotate?key=AIzaSyAdOQ60IMlOQwxiU37ykZRHOg2QB6_i2x0")
    String validateCredentials(String request);

}