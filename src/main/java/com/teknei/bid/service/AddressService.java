package com.teknei.bid.service;

import com.teknei.bid.dto.DocumentPictureRequestDTO;
import com.teknei.bid.persistence.entities.BidClie;
import com.teknei.bid.persistence.entities.BidClieDire;
import com.teknei.bid.persistence.entities.BidClieDireCont;
import com.teknei.bid.persistence.entities.BidClieDireNest;
import com.teknei.bid.persistence.repository.BidClieDireContRepository;
import com.teknei.bid.persistence.repository.BidClieRepository;
import com.teknei.bid.persistence.repository.BidDireNestRepository;
import com.teknei.bid.persistence.repository.BidDireRepository;
import com.teknei.bid.util.tas.TasManager;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Services related to the address
 */
@Service
public class AddressService {

    @Autowired
    private TasManager tasManager;
    @Autowired
    private BidDireNestRepository direNestRepository;
    @Autowired
    private BidDireRepository direRepository;
    @Autowired
    private BidClieDireContRepository direContRepository;
    @Autowired
    private BidClieRepository clieRepository;
    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yy");
    private static final Logger log = LoggerFactory.getLogger(AddressService.class);

    /**
     * Finds the address picture related to the process
     *
     * @param requestDTO - The request
     * @return - The image
     */
    public byte[] findPicture(DocumentPictureRequestDTO requestDTO) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".findPicture ");
        return tasManager.getImage(requestDTO.getId());
    }

    public void syncWithPreloaded(Long id, String username) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".syncWithPreloaded ");
        BidClieDireCont direCont = null;
        try {
            direCont = direContRepository.findTopByIdClieAndIdEsta(id, 1);
            if (direCont == null) {
                log.info("No address preloaded for id: {}", id);
                log.info("Skipping");
                return;
            }
            BidClie clie = clieRepository.findOne(id);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Nombre", new StringBuilder(clie.getNomClie()).append(" ").append(clie.getApePate() == null ? "" : clie.getApePate()).append(" ").append(clie.getApeMate() == null ? "" : clie.getApeMate()).toString());
            jsonObject.put("TipoDocumento", "Provided");
            jsonObject.put("Fecha", sdf.format(new Date(System.currentTimeMillis())));
            jsonObject.put("Direccion", direCont.getDomi());
            jsonObject.put("Referencia", UUID.randomUUID().toString());
            
            log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".syncWithPreloaded id:"+id);
            log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".syncWithPreloaded Nombre:"+new StringBuilder(clie.getNomClie()).append(" ").append(clie.getApePate() == null ? "" : clie.getApePate()).append(" ").append(clie.getApeMate() == null ? "" : clie.getApeMate()).toString());
            log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".syncWithPreloaded TipoDocumento:Provided");
            log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".syncWithPreloaded Fecha:"+ sdf.format(new Date(System.currentTimeMillis())));
            log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".syncWithPreloaded Direccion:"+direCont.getDomi());
            
            BidClieDireNest direNest = new BidClieDireNest();
            direNest.setIdClie(id);
            direNest.setIdDire(id);
            direNest.setDirNestUno(jsonObject.toString());
            direNest.setDirNestDos(direCont.getDomi());
            direNest.setUsrOpeCrea(username);
            direNest.setUsrCrea(username);
            direNest.setFchCrea(new Timestamp(System.currentTimeMillis()));
            direNest.setIdEsta(1);
            direNest.setIdTipo(3);
            direNestRepository.save(direNest);
            BidClieDire clieDire = new BidClieDire();
            clieDire.setCp(direCont.getCp());
            clieDire.setUsrCrea(username);
            clieDire.setUsrOpeCrea(username);
            clieDire.setIdTipo(3);
            clieDire.setIdEsta(1);
            clieDire.setIdClie(id);
            clieDire.setFchCrea(new Timestamp(System.currentTimeMillis()));
            clieDire.setCall(direCont.getDomi());
            clieDire.setCol(direCont.getCol());
            clieDire.setIdDire(id);
            direRepository.save(clieDire);
        } catch (Exception e) {
            log.error("Error sync with preloaded with message: {}", e.getMessage());
        }

    }

}
