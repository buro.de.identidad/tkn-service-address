package com.teknei.bid.command.impl.address;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.dto.AddressDetailDTO;
import com.teknei.bid.persistence.entities.*;
import com.teknei.bid.persistence.repository.BidCurpRepository;
import com.teknei.bid.persistence.repository.BidDireNestRepository;
import com.teknei.bid.persistence.repository.BidDireRepository;
import com.teknei.bid.persistence.repository.BidRegProcRepository;
import com.teknei.bid.service.AddressService;
import com.teknei.bid.util.DireParseUtil;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

/**
 * Specialization of the command. Executes only the 'save to db' part of the process
 */
@Component
public class PersistAddressCommand implements Command {

    private static final Logger log = LoggerFactory.getLogger(PersistAddressCommand.class);

    @Autowired
    private BidRegProcRepository bidRegProcRepository;
    @Autowired
    private BidCurpRepository bidCurpRepository;
    @Autowired
    private BidDireNestRepository bidDireNestRepository;
    @Autowired
    private BidDireRepository bidDireRepository;
    @Autowired
    private AddressService addressService;

    @Override
    public CommandResponse execute(CommandRequest request) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".execute");
        CommandResponse response = new CommandResponse();
        response.setScanId(request.getScanId());
        response.setDocumentId(request.getDocumentId());
        response.setId(request.getId());
        try {
            if (request.getData().equals("OCR-NA")) {
                addressService.syncWithPreloaded(request.getId(), request.getUsername());
            } else {
                JSONObject requestJson = new JSONObject(request.getData());
                addDb(request.getId(), requestJson.toString(), requestJson.optString("Direccion", requestJson.toString()), request.getUsername());
            }
            response.setDesc(String.valueOf(Status.ADDRESS_DB_OK));
            response.setStatus(Status.ADDRESS_DB_OK);
        } catch (Exception e) {
            log.error("Error in PersistAddressCommand: {}", e.getMessage());
            response.setStatus(Status.ADDRESS_DB_ERROR);
            response.setDesc(e.getMessage());
        }
        return response;
    }

    /**
     * Adds to the db the address without parsing process and updates the status of the main process
     *
     * @param idCustomer
     * @param fullResponse
     * @param addressPart
     */
    private void addDb(Long idCustomer, String fullResponse, String addressPart, String username) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".addDb ");
        registerAddress(idCustomer, fullResponse, addressPart, username);
        alterRecord(idCustomer, true, username);
    }

    /**
     * Registers the address in the db, parsed and not parsed
     *
     * @param idCustomer
     * @param fullResponse
     * @param addressPart
     */
    private void registerAddress(Long idCustomer, String fullResponse, String addressPart, String username) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".registerAddress");
        BidClieDireNestPK nestPK = new BidClieDireNestPK();
        nestPK.setIdClie(idCustomer);
        nestPK.setIdDire(idCustomer);
        BidClieDireNest retrieved = bidDireNestRepository.findOne(nestPK);
        if (retrieved == null) {
            saveAddress(idCustomer, fullResponse, addressPart, username);
        } else {
            updateAddress(retrieved, fullResponse, addressPart, username);
        }
        BidClieDirePK direPK = new BidClieDirePK();
        direPK.setIdClie(idCustomer);
        direPK.setIdDire(idCustomer);
        BidClieDire bidClieDire = bidDireRepository.findOne(direPK);
        if (bidClieDire == null) {
            saveDireFull(fullResponse, idCustomer, username);
        } else {
            updateDireFull(fullResponse, bidClieDire, username);
        }
    }

    private void saveAddress(Long idCustomer, String fullResponse, String addressPart, String username) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".saveAddress");
        BidClieDireNest direNest = new BidClieDireNest();
        direNest.setIdClie(idCustomer);
        direNest.setIdDire(idCustomer);
        direNest.setDirNestUno(fullResponse);
        direNest.setDirNestDos(addressPart);
        direNest.setUsrOpeCrea(username);
        direNest.setUsrCrea(username);
        direNest.setFchCrea(new Timestamp(System.currentTimeMillis()));
        direNest.setIdEsta(1);
        direNest.setIdTipo(3);
        bidDireNestRepository.save(direNest);
    }

    private void updateAddress(BidClieDireNest retrieved, String fullResponse, String addressPart, String username) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".updateAddress");
        retrieved.setDirNestUno(fullResponse);
        retrieved.setDirNestDos(addressPart);
        retrieved.setUsrOpeModi(username);
        retrieved.setUsrModi(username);
        retrieved.setFchModi(new Timestamp(System.currentTimeMillis()));
        bidDireNestRepository.save(retrieved);
    }


    /**
     * Saves the address in the db. Only parsed
     *
     * @param addressOne
     * @param id
     */
    private void saveDireFull(String addressOne, Long id, String username) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".saveDireFull");
        AddressDetailDTO dto = DireParseUtil.parseDetail(addressOne);
        if (dto != null) {
            BidClieDire bidClieDire = new BidClieDire();
            bidClieDire.setNoInt(dto.getIntNumber());
            bidClieDire.setNoExt(dto.getExtNumber());
            if (dto.getZipCode().length() <= 5) {
                bidClieDire.setCp(dto.getZipCode());
            }
            bidClieDire.setCol(dto.getSuburb());
            bidClieDire.setCall(dto.getStreet());
            bidClieDire.setIdClie(id);
            bidClieDire.setIdDire(id);
            bidClieDire.setIdEsta(1);
            bidClieDire.setIdTipo(3);
            bidClieDire.setUsrCrea(username);
            bidClieDire.setUsrOpeCrea(username);
            bidClieDire.setFchCrea(new Timestamp(System.currentTimeMillis()));
            try {
                bidDireRepository.save(bidClieDire);
            } catch (Exception e) {
                log.error("Error saving address with full details for: {} with message: {}", addressOne, e.getMessage());
            }
        }
        log.info("Done save dire full");
    }

    /**
     * Updates the address in db. Only parsed
     *
     * @param addressOne
     * @param bidClieDire
     */
    private void updateDireFull(String addressOne, BidClieDire bidClieDire, String username) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".updateDireFull");
        AddressDetailDTO dto = DireParseUtil.parseDetail(addressOne);
        if (dto != null) {
            bidClieDire.setNoInt(dto.getIntNumber());
            bidClieDire.setNoExt(dto.getExtNumber());
            if (dto.getZipCode().length() <= 5) {
                bidClieDire.setCp(dto.getZipCode());
            }
            bidClieDire.setCol(dto.getSuburb());
            bidClieDire.setCall(dto.getStreet());
            bidClieDire.setUsrModi(username);
            bidClieDire.setUsrOpeModi(username);
            bidClieDire.setFchModi(new Timestamp(System.currentTimeMillis()));
        }
        try {
            bidDireRepository.save(bidClieDire);
        } catch (Exception e) {
            log.error("Error updating address with full details for: {} with message: {}", addressOne, e.getMessage());
        }
    }

    /**
     * Updates the process status for the main flow
     *
     * @param operationId
     * @param created
     */
    private void alterRecord(Long operationId, boolean created, String username) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".alterRecord ");
        BidClieCurp curp = bidCurpRepository.findTopByIdClie(operationId);
        BidClieRegProcPK pk = new BidClieRegProcPK();
        pk.setIdClie(operationId);
        pk.setCurp(curp.getCurp());
        BidClieRegProc proc = bidRegProcRepository.findOne(pk);
        proc.setRegDomi(created);
        proc.setUsrOpeModi(username);
        proc.setFchModi(new Timestamp(System.currentTimeMillis()));
        proc.setUsrModi(username);
        proc.setFchRegDomi(new Timestamp(System.currentTimeMillis()));
        bidRegProcRepository.save(proc);
    }
}
