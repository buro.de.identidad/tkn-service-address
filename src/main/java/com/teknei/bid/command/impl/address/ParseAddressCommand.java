package com.teknei.bid.command.impl.address;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Specialization of command. Executes only parsing services
 */
@Component
public class ParseAddressCommand implements Command {

    private static final Logger log = LoggerFactory.getLogger(ParseAddressCommand.class);

    @Value("#${tkn.kofax.outputFolder}")
    private String documentFolderUrlOutput;
    @Value("#${tkn.kofax.inputFolder}")
    private String documentFolderUrlInput;
    @Value("${tkn.kofax.maxSecondsWait}")
    private Integer maxSecondsToWaitDocumentProperties;

    @Override
    public CommandResponse execute(CommandRequest request) 
    {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".execute");
        CommandResponse response = new CommandResponse();
        response.setId(request.getId());
        response.setScanId(request.getScanId());
        response.setDocumentId(request.getDocumentId());
        try {
            JSONObject jsonObject = addAddressDocument(request.getFileContent().get(0), String.valueOf(request.getId()));
            if (jsonObject == null) {
                response.setDesc("Error parsing KOFAX capture");
                response.setStatus(Status.ADDRESS_KOFAX_ERROR);
            } else {
                response.setDesc(jsonObject.toString());
                response.setStatus(Status.ADDRESS_KOFAX_OK);
            }
        } catch (Exception e) {
            log.error("Error in ParseAddressCommmand: {}", e.getMessage());
            response.setStatus(Status.ADDRESS_KOFAX_ERROR);
            response.setDesc(e.getMessage());
        } finally {
            cleanOutputFolder(String.valueOf(request.getId()));
        }
        return response;
    }

    private void cleanOutputFolder(String personalNumberIdentification) 
    {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".cleanOutputFolder ");
        String filenameOutputJpeg = personalNumberIdentification + ".jpeg";
        String filenameOutputCsv = personalNumberIdentification + ".csv";
        String fullURIOutputJpegFile = documentFolderUrlInput + filenameOutputJpeg;
        String fullURIOutputCsvFile = documentFolderUrlInput + filenameOutputCsv;
        fullURIOutputJpegFile = fullURIOutputJpegFile.trim().replace("#", "");
        fullURIOutputCsvFile = fullURIOutputCsvFile.trim().replace("#", "");
        Path pathOutputJpeg = Paths.get(fullURIOutputJpegFile);
        Path pathOutputCsv = Paths.get(fullURIOutputCsvFile);
        try {
            Files.delete(pathOutputCsv);
        } catch (IOException e) {
            log.error("Error clean path: {} with message: {}", pathOutputCsv.toString(), e.getMessage());
        }
        try {
            Files.delete(pathOutputJpeg);
        } catch (IOException e) {
            log.error("Error clean path: {} with message: {}", pathOutputJpeg.toString(), e.getMessage());
        }
    }

    /**
     * Puts the image on the required folder in order to Kofax processes it
     *
     * @param imageData                    - the image
     * @param personalNumberIdentification - the customer personal number (ie. INE number). Its really not important by the flow, its just only a temporal identifier
     * @return The address parsed as JSON
     * @throws IOException
     * @throws URISyntaxException
     */
    private JSONObject addAddressDocument(byte[] imageData, String personalNumberIdentification) throws IOException, URISyntaxException {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".addAddressDocument ");
        JSONObject jsonObject = null;
        String filenameOutput = personalNumberIdentification + ".jpeg";
        String filenameInput = personalNumberIdentification + ".csv";
        String fullURIOutputFile = documentFolderUrlOutput + filenameOutput;
        String fullURIInputFile = documentFolderUrlInput + filenameInput;
        fullURIOutputFile = fullURIOutputFile.trim().replace("#", "");
        fullURIInputFile = fullURIInputFile.trim().replace("#", "");
        Path pathOutput = Paths.get(fullURIOutputFile);
        Path pathInput = Paths.get(fullURIInputFile);
        Files.write(pathOutput, imageData);
        int maxCounter = maxSecondsToWaitDocumentProperties / 3;
        for (int i = 0; i <= maxCounter; i++) {
            if (Files.exists(pathInput)) {
                List<String> listLines = Files.readAllLines(pathInput);
                String firstLine = listLines.get(0);
                String lastLine = listLines.get(1);
                String[] firstParsed = firstLine.split(",");
                String[] secondParsed = lastLine.split(",");
                jsonObject = new JSONObject();
                try {
                    for (int j = 0; j < firstParsed.length; j++) {
                        jsonObject.put(firstParsed[j], secondParsed[j]);
                    }
                } catch (Exception e) {
                    log.error("Error from Kofax csv parsing: {}", e.getMessage());
                }
                return jsonObject;
            }
            try {
                Thread.sleep(3 * 1000);
            } catch (InterruptedException e) {
            }
        }
        log.warn("Max time reached waiting for CSV file, returning null");
        return null;
    }
}
