package com.teknei.bid.command.impl.address;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.service.remote.ByPassCaller;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Specialization of command. Executes only parsing services
 */
@Component
public class ParseAddressProxyCommand implements Command {

    private static final Logger log = LoggerFactory.getLogger(ParseAddressProxyCommand.class);
    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy");

    @Autowired
    private ByPassCaller visionCaller;

    @Override
    public CommandResponse execute(CommandRequest request) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".execute ");
        CommandResponse response = new CommandResponse();
        response.setId(request.getId());
        response.setScanId(request.getScanId());
        response.setDocumentId(request.getDocumentId());
        try {
            JSONObject jsonObject = addAddressDocument(request.getFileContent().get(0), String.valueOf(request.getId()));
            if (jsonObject == null) {
                response.setDesc("Error parsing VISION capture");
                response.setStatus(Status.ADDRESS_KOFAX_ERROR);
            } else {
                response.setDesc(jsonObject.toString());
                response.setStatus(Status.ADDRESS_KOFAX_OK);
            }
        } catch (Exception e) {
            log.error("Error in ParseAddressVisionCommmand: {}", e.getMessage());
            response.setStatus(Status.ADDRESS_KOFAX_ERROR);
            response.setDesc(e.getMessage());
        }
        return response;
    }

    /**
     * Puts the image on the required folder in order to Kofax processes it
     *
     * @param imageData                    - the image
     * @param personalNumberIdentification - the customer personal number (ie. INE number). Its really not important by the flow, its just only a temporal identifier
     * @return The address parsed as JSON
     * @throws IOException
     * @throws URISyntaxException
     */
    private JSONObject addAddressDocument(byte[] imageData, String personalNumberIdentification) throws IOException, URISyntaxException {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".addAddressDocument");
        JSONObject featureRequest = new JSONObject();
        JSONArray featureRequestArray = new JSONArray();
        JSONArray requestsArray = new JSONArray();
        featureRequestArray.put(featureRequest);
        featureRequest.put("type", "TEXT_DETECTION");
        JSONObject jsonRequest = new JSONObject();
        String b64Img = null;
        try {
            b64Img = Base64Utils.encodeToString(imageData);
        } catch (Exception e) {
            log.error("Error coding to B64 image from request");
            return null;
        }
        JSONObject imageRequest = new JSONObject();
        imageRequest.put("content", b64Img);
        jsonRequest.put("image", imageRequest);
        jsonRequest.put("features", featureRequestArray);
        requestsArray.put(jsonRequest);

        JSONObject mainRequest = new JSONObject();
        mainRequest.put("requests", requestsArray);
        String mainStringRequest = mainRequest.toString();
        String responseVision = visionCaller.validateCredentials(mainStringRequest);
        JSONObject jsonVisionResponse = new JSONObject(responseVision);
        JSONArray jsonArrayResponse = jsonVisionResponse.getJSONArray("responses");
        List<String> listResponses = new ArrayList<>();
        for (int i = 0; i < jsonArrayResponse.length(); i++) {
            JSONObject jsonLocalResponse = jsonArrayResponse.getJSONObject(i);
            JSONObject fullTextResponse = jsonLocalResponse.getJSONObject("fullTextAnnotation");
            String finalStringResponse = fullTextResponse.getString("text");
            listResponses.add(finalStringResponse);
        }
        String raw = listResponses.get(0);
        JSONObject mainResponse = null;
        if (raw.contains("CFE") || raw.contains("Electricidad")) {
            mainResponse = parseResponseCFE(listResponses.get(0));
        } else if (raw.contains("TELMEX") || raw.contains("TELEFONOS DE MEXICO")) {
            mainResponse = parseResponseCFE(listResponses.get(0));
        } else {
            mainResponse = parseResponseCFE(listResponses.get(0));
        }
        return mainResponse;
    }

    private JSONObject parseResponseCFE(String response) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".parseResponseCFE ");
        String name = parseNameCFE(response);
        String dateCfe = parseDateCFE(response);
        String addressCfe = parseAddressCFE(response);
        String referenceCfe = parseReferenceCFE(response);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Nombre", name);
        jsonObject.put("TipoDocumento", "CFE");
        jsonObject.put("Fecha", dateCfe);
        jsonObject.put("Direccion", addressCfe);
        jsonObject.put("Referencia", referenceCfe);
        jsonObject.put("CFE", "CFE");
        log.info("Returning: {}", jsonObject);
        return jsonObject;
    }

    private String parseReferenceCFE(String response) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".parseReferenceCFE ");
        String regexPrimary = "\\d{3}\\s\\d{3}\\s\\d{3}\\s\\d{3}";
        String secondary = "\\d{3}\\s\\d{3}\\s\\d{3}";
        String fallback = "\\d{3}\\s\\d{3}";
        List<String> arrayRegex = Arrays.asList(regexPrimary, secondary, fallback);
        for (String s :
                arrayRegex) {
            Pattern pattern = Pattern.compile(s);
            Matcher matcher = pattern.matcher(response);
            int posBegin = 0;
            int posEnd = 0;
            if (matcher.find()) {
                posBegin = matcher.start();
                posEnd = matcher.end();
                response = response.substring(posBegin, posEnd);
                return response;
            }
        }
        return null;
    }

    private String parseAddressCFE(String response) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".parseAddressCFE ");
        String tagBegin = parseNameCFE(response);
        String tagEndRegex = "\\d{2,3}\\s\\d{2,3}\\s\\d{2,3}";
        if (tagBegin != null) {
            response = response.substring(response.indexOf(tagBegin));
            response = response.replace(tagBegin, "");
            response = response.trim();
            if (response.startsWith("\n") || response.startsWith("\\n")) {
                response = response.substring(2);
            }
            Pattern pattern = Pattern.compile(tagEndRegex);
            Matcher matcher = pattern.matcher(response);
            List<String> listAddressPars = new ArrayList<>();
            if (matcher.find()) {
                int pos = matcher.start();
                response = response.substring(0, pos);
                if (response.endsWith("\n") || response.endsWith("\\n")) {
                    response = response.substring(0, response.length() - 3);
                }
                response = response.replace("\n", "|");
                response = response.replace("\\n", "|");
                String[] parsed = response.split("\\|");
                Pattern pattern1 = Pattern.compile("[[:lower:]]{3,}");
                Pattern pattern2 = Pattern.compile("\\d\\.\\d");
                String cpRegex = "C.P.\\s\\d{5}";
                Pattern patternCP = Pattern.compile(cpRegex);
                for (String s :
                        parsed) {
                    Matcher matcher1 = pattern1.matcher(s);
                    Matcher matcher2 = pattern2.matcher(s);
                    if (!matcher1.find() && !matcher2.find()) {
                        boolean isNumeric = s.chars().allMatch(Character::isDigit);
                        if (!isNumeric) {
                            Matcher matcher3 = patternCP.matcher(s);
                            if (matcher3.find()) {
                                int beginPosCp = matcher3.start();
                                int endPosCp = matcher3.end();
                                String cp = s.substring(beginPosCp, endPosCp);
                                s = s.substring(0, beginPosCp);
                                listAddressPars.add(s);
                                cp = cp.replace("C.P.", "CP:");
                                listAddressPars.add(cp);
                            } else {
                                listAddressPars.add(s);
                            }
                        }
                    }
                }
            }
            String lastSegment = listAddressPars.get(listAddressPars.size() - 1);
            if (lastSegment.contains(",")) {
                lastSegment = lastSegment.replace(",", "-");
                listAddressPars.remove(listAddressPars.size() - 1);
                listAddressPars.add(lastSegment);
                String[] municipalityArray = lastSegment.split("-");
                String municipality = municipalityArray[0];
                boolean flag = false;
                for (String s : listAddressPars) {
                    if (s.contains("CP:")) {
                        flag = true;
                    }
                }
                if (flag) {
                    listAddressPars.add(2, municipality);
                }
            }
            StringBuilder stringBuilder = new StringBuilder();
            listAddressPars.forEach(a -> stringBuilder.append(a).append("-"));
            String address_1 = stringBuilder.toString();
            address_1 = address_1.substring(0, address_1.length() - 1);
            address_1 = address_1.replace("\n", "-");
            return address_1;
        }
        String tagEnd = "Número de servicio";
        //TODO inverse
        return null;
    }

    private String parseDateCFE(String response) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".parseDateCFE ");
        String regex = "\\d\\d\\s\\D\\D\\w\\s\\d\\d";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(response);
        int begin = 0;
        int end = 0;
        boolean found = true;
        String dateString = null;
        while (matcher.find()) {
            begin = matcher.start();
            end = matcher.end();
        }
        if (found) {
            dateString = response.substring(begin, end);
        } else {
            return null;
        }
        dateString.replaceAll("\n", "");
        dateString.replaceAll(" ", "-");
        return dateString;
    }

    private String parseNameCFE(String response) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".parseNameCFE ");
        String[] tokens = {"Nombre y Domicilio", "Nombre", "Domicio", "Nombrey", "ombre"};
        boolean foundToken = false;
        for (String t :
                tokens) {
            if (response.contains(t)) {
                response = response.substring(response.indexOf(t));
                foundToken = true;
                break;
            }
        }
        if (foundToken) {
            if (response.startsWith("\n") || response.startsWith("\\n")) {
                response = response.substring(2);
            }
            response = response.trim();

            int toEnd = response.indexOf("\n");
            if (toEnd == -1) {
                toEnd = response.indexOf("\\n");
            }
            response = response.substring(toEnd);
            response = response.trim();
            if (response.startsWith("\n") || response.startsWith("\\n")) {
                response = response.substring(2);
            }
            String[] numberWords = {"UNO", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE", "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE", "VEINTE", "TREINTA", "CUARENTA", "CINCUENTA", "SESENTA", "SETENTA", "OCHENTA", "NOVENTA", "CIEN", "CIENTO", "PESO", "Y"};
            int counter = 0;
            for (String s :
                    numberWords) {
                if (response.contains(s)) {
                    counter++;
                }
                if (counter >= 3) {
                    int pos = response.indexOf("\n");
                    if (pos == -1) {
                        pos = response.indexOf("\\n");
                    }
                    response = response.substring(pos);
                    break;
                }
            }
            if (response.startsWith("\n") || response.startsWith("\\n")) {
                response = response.substring(2);
            }
            response = response.trim();
            int endIndex = response.indexOf("\n");
            if (endIndex == -1) {
                endIndex = response.indexOf("\\n");
            }
            response = response.substring(0, endIndex);
            return response;
        } else {
            return null;
        }
    }
}
